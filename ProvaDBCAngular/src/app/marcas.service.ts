import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {

  url = 'http://localhost:8080/api/marcas';
  constructor(private http: HttpClient) { }

  public gerMarcas():Observable<any[]>{
    return this.http.get<any[]>(this.url);
  }
}
