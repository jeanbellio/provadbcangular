import { Component, OnInit } from '@angular/core';
import { MarcasService } from '../marcas.service';

@Component({
  selector: 'marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.css']
})
export class MarcasComponent implements OnInit {

  marcas:any;
  constructor(private marcasService:MarcasService) { }

  ngOnInit() {
    //this.buscarMarcas();
  }

  buscarMarcas(){
    this.marcasService.gerMarcas().subscribe(
      response => {
        this.marcas = response;
      }
    )
  }

}
